#FROM tiangolo/uwsgi-nginx-flask:python3.8
FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7

#RUN addgroup -g 2000 node && adduser -u 2000 -G node -s /bin/sh -D node

WORKDIR /usr/src/app

ENV STATIC_PATH /var/www/app/static
ENV STATIC_URL /static

EXPOSE 11130
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

COPY --chown=2000:2000 app.py /usr/src/app 
CMD [ "flask", "run","--host","0.0.0.0","--port","11130"]
#USER 2000
